﻿/***********************************
 * **** Eswin Palacios Sarmiento
 **********************************/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @class
{
    class Program
    {
        static void Main(string[] args)
        {
            Crypt crypt = new Crypt("abcdefghijklmnopqrstuvwxyz.@", "wunderman");
            Console.WriteLine(crypt.decipher("ggvvihkcny@n@dlenq.jzdrpooz"));
            Console.WriteLine(crypt.cipher("moises.cachay@wunderman.com"));
            Console.ReadKey();
        }
    }

    class Crypt : Vigenere
    { 
        private string keyword;
		private string alphabet;
		private string relative;

        public Crypt(string alphabet, string keyword)
        {
            this.alphabet = alphabet;
	      	this.keyword = keyword;
        }

        private void shift(string plaintext)
        {
            double result = Math.Ceiling( Convert.ToDouble( plaintext.Length / this.keyword.Length ));

            for (int i = 0; i < result; i++)
                this.relative += this.keyword;		
			
            this.relative = this.relative.Substring(0, plaintext.Length);
        }

        public string cipher(string plaintext)
        {
            return this.process(plaintext, "c");
        }

        public string decipher(string plaintext)
        {
            return this.process(plaintext, "d");
        }

        private string process(string text, string type)
        {
            this.shift(text);
            string text_decipher = "";
            int sum = 0;

            for (int i = 0; i < text.Length; i++)
            {
                if (type == "d")
                { 
                    sum = this.alphabet.IndexOf(text[i]) - this.alphabet.IndexOf(this.relative[i]);
                    sum += this.alphabet.Length;
                }
                else
                    sum = this.alphabet.IndexOf(text[i]) + this.alphabet.IndexOf(this.relative[i]);

                int mod = sum % this.alphabet.Length;
                text_decipher += this.alphabet[mod];
            }

            return text_decipher;
        }
    }  

    interface Vigenere {
        string cipher(string plaintext);
        string decipher(string ciphertext);
    }
}
