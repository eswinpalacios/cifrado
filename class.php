<?php
	/******************************************
	***** Eswin Alexander Palacios Sarmiento
	***** Method 1
	******************************************/

	interface Vigenere {	

		public function shift($plaintext);
		public function cipher($plaintext);
		public function decipher($ciphertext);
	}

	class Crypt implements Vigenere
	{
		private $keyword;
		private $alphabet;
		private $relative;

		function __construct($alphabet, $keyword)
	   	{
	      	$this->alphabet = $alphabet;
	      	$this->keyword = $keyword;
	   	}

		public function shift($plaintext)
		{
			$result = ceil(strlen($plaintext)/strlen($this->keyword));
			
			for ($i=0; $i < $result; $i++) { 
				$this->relative .= $this->keyword;
			}

			$this->relative = substr($this->relative, 0, strlen($plaintext));
		}

		public function cipher($plaintext)
		{
			$this->shift($plaintext);
			$text_decipher = "";
			
			for ($i=0; $i < strlen($plaintext); $i++)
				$text_decipher .= $this->cipher_caracter($plaintext[$i], $i);

			return $text_decipher;
		}

		public function decipher($ciphertext)
		{
			$this->shift($ciphertext);
			$text_decipher = "";
			
			for ($i=0; $i < strlen($ciphertext); $i++)
				$text_decipher .= $this->decipher_caracter($ciphertext[$i], $i);

			return $text_decipher;
		}

		private function decipher_caracter($c, $i)
		{		
			$sum = strpos($this->alphabet, $c) - strpos($this->alphabet, $this->relative[$i]) ;
			$sum += strlen($this->alphabet);
			$modulo = $sum  % strlen($this->alphabet);
			
			return $this->alphabet[$modulo];
		}

		private function cipher_caracter($c, $i)
		{		
			$sum = strpos($this->alphabet, $c) + strpos($this->alphabet, $this->relative[$i]) ;
			$modulo = $sum  % strlen($this->alphabet);

			return $this->alphabet[$modulo];
		}

	}

	//exercise!

	$crypt = new Crypt("abcdefghijklmnopqrstuvwxyz.@", "wunderman");
	var_dump($crypt->decipher("ggvvihkcny@n@dlenq.jzdrpooz"));
	var_dump($crypt->cipher("moises.cachay@wunderman.com"));

	exit();

?>