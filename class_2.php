<?php
	/******************************************
	***** Eswin Alexander Palacios Sarmiento
	***** Method 2
	******************************************/

	interface Vigenere {	

		public function shift($plaintext);
		public function cipher($plaintext);
		public function decipher($ciphertext);
	}

	class Crypt implements Vigenere
	{
		private $keyword;
		private $alphabet;
		private $relative;

		function __construct($alphabet, $keyword)
	   	{
	      	$this->alphabet = $alphabet;
	      	$this->keyword = $keyword;
	   	}

		public function shift($plaintext)
		{
			$result = ceil(strlen($plaintext)/strlen($this->keyword));
			
			for ($i=0; $i < $result; $i++) { 
				$this->relative .= $this->keyword;
			}

			$this->relative = substr($this->relative, 0, strlen($plaintext));
		}

		public function cipher($plaintext)
		{
			return $this->process($plaintext,"c");
		}

		public function decipher($ciphertext)
		{
			return $this->process($ciphertext,"d");
		}

		private function process($text, $type)
		{
			$this->shift($text);
			$text_decipher = "";
			
			for ($i=0; $i < strlen($text); $i++)
			{
				if($type == "d"){
					$sum = strpos($this->alphabet, $text[$i]) - strpos($this->alphabet, $this->relative[$i]) ;
					$sum += strlen($this->alphabet);	
				}
				else
					$sum = strpos($this->alphabet, $text[$i]) + strpos($this->alphabet, $this->relative[$i]) ;
				
				$modulo = $sum  % strlen($this->alphabet);
				$text_decipher .= $this->alphabet[$modulo];
			}			

			return $text_decipher;
		}

	}

	//exercise!

	$crypt = new Crypt("abcdefghijklmnopqrstuvwxyz.@", "wunderman");

	var_dump($crypt->decipher("ggvvihkcny@n@dlenq.jzdrpooz","d"));
	var_dump($crypt->cipher("moises.cachay@wunderman.com","c"));

	exit();

?>